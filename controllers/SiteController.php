<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productos;
use yii\data\ActiveDataProvider;
use app\controllers\CategoriasController;
use app\models\Categorias;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * actions controla las acciones que no existen
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
    
        return $this->render('inicio',[
           "titulo"=>"Tiendas JOTA",
            "foto"=>"bici.png",
            "texto"=>"Más de 20 años de experiencia"
        ]);
    }
    
    
    public function actionOfertas()
    {
        
        //solo los que oferta true
         $consulta=Productos::find()
                 //->where("oferta = 1")
                 ->where(["oferta"=>1])
                 ->all();
 
            return $this->render('productos',[
                'productos'=>$consulta,
            ]);
            
            
            
    }
    
     public function actionProductos()
    {
         //Devuelve un array de productos Es un activeQuery
          $consulta= Productos::find()->all();
          // Como voy a mostrar los productos con un array le paso la consulta ejecutada
          //Le estoy pasando el array de objetos con los productos
            return $this->render('productos',[
                'productos'=>$consulta,
            ]);
    }
    
     public function actionCategorias()
    {
         
        $dataProvider = new ActiveDataProvider([
            'query' => Categorias::find(),
        ]);
        //Aquí voy a usar el GridView y éste necesita la consulta sin ejecutar
        return $this->render('categorias', [
            'categorias' => $dataProvider,
        ]);
    }
    
    
     public function actionEstamos()
    {
        return $this->render('estamos');
    }
    
     public function actionSomos()
    {
        return $this->render('somos');
    }
    
     public function actionNuestros()
    {
        return $this->render('nuestros');
    }
    
     public function actionInformacion()
    {
        return $this->render('informacion');
    }
    
      public function actionContacto()
    {
        return $this->render('contacto');
    }
    
    
       public function actionFotodetalle($id)
    {
           
        /*$dataProvider = new ActiveDataProvider([
            'query' => Categorias::find()->where(["id"=>$id]),
        ]);*/
        
        return $this->render('fotodetalle',[
            'model' => Categorias::findOne($id),
        ]);
    }

    
    
    
    
    
}
