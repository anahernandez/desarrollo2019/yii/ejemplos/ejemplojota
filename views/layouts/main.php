<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use rmrevin\yii\fontawesome\FA;





AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    
    
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        //Esto es para que traduzca las etiquetas de los iconos
        'encodeLabels' => false,
        'items' => [
            [
               
                'label' => FA::icon('home').'&nbsp&nbsp'.'Inicio', 
                'url' => ['/site/inicio']
            ],
            
            [
                'label' => FA::icon('euro').'&nbsp&nbsp'.'Ofertas',
                'url' => ['/site/ofertas']],
            [
                'label' => FA::icon('book').'&nbsp&nbsp'.'Productos',
                'url' => ['/site/productos']
                ],
            [
                'label' => FA::icon('sitemap').'&nbsp&nbsp'.'Categorías',
                'url' => ['/site/categorias']
                ],
            
            /* desplegable con yii2*/
         
           [
            'label' => FA::icon('users').'&nbsp&nbsp'.'Nosotros',
            'items' => [
                ['label' => 'Dónde Estamos', 'url' => ['/site/estamos']],
                ['label' => 'Quienes somos', 'url' => ['/site/somos']],
                ['label' => 'Nuestros productos', 'url' => ['/site/nuestros']],
                 '<li class="divider"></li>',
                ['label' => 'Información', 'url' => ['/site/informacion']],
            ],
        ], 
        
            ['label' => FA::icon('phone').'&nbsp&nbsp'.'Contacto', 'url' => ['/site/contacto']],
           
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; JOTA - Ana Hernández <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
