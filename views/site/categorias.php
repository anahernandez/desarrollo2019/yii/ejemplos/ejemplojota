<?php
    use yii\grid\GridView;
    use yii\helpers\Html;
    

    echo GridView::widget([
        'dataProvider' => $categorias,
        'columns' => [
            'id',
            'nombre',
            //'descripcion',
            [
                'label'=>'descripcion',
                'value'=>function($model){
                    return mb_substr($model->descripcion, 0, 5);
                }
            ],
      
            
            [
                'label'=>'Foto del producto',
                'format'=>'raw',
                'value'=> function($categorias){
                return Html::a(Html::img('@web/imgs/'.$categorias->foto,[
                    'class'=>'fotogrid'
                ]),["site/fotodetalle","id"=>$categorias->id]);
            }
     
            
                
            ]
        ],
    ]); ?>

