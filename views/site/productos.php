<?php

/* @var $this yii\web\View */

$this->title = 'Productos';
?>
<div class="container-fluid">
<div class="row row-flex row-flex-wrap">
  <?php
   
    foreach($productos as $k=>$producto){
        echo $this->render("_productos",[
            "id"=>$producto->id,
            "nombre"=>$producto->nombre,
            "foto"=>$producto->foto,
            "descripcion"=>$producto->descripcion,
            "precio"=>$producto->precio,
        ]);
    }
  ?>
</div>
</div>



