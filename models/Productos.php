<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $foto
 * @property string|null $descripcion
 * @property int|null $precio
 * @property int|null $oferta
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'precio', 'oferta'], 'integer'],
            [['nombre', 'foto'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'oferta' => 'Oferta',
        ];
    }
    
    
    
    public static function mostrarUna(){
        /**
         * numero total de registros
         */
        $total=self::find()->count();
        /**
         * Calculo una posicion aleatoria
         */
        $n=rand(0, $total-1);
        
        return self::find()->offset($n)->limit(1)->one();
             
    }
}
